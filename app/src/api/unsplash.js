import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID qGnVBwITHyaxET404uQ6EkEO2VbGnjoTo_0COsgbFeo'
    }
});